import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AppNavBar from './components/AppNavBar'
import HomePage from './components/HomePage'
import ProjectPage from './components/ProjectPage'
import ContactPage from './components/ContactPage'
import './index.css';

const pageComponent = (

	<React.Fragment>
		
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={HomePage}/>
				<Route exact path="/projects" component={ProjectPage}/>
				<Route exact path="/contact" component={ContactPage}/>
			</Switch>
		</BrowserRouter>
		
	</React.Fragment>

)

ReactDOM.render( pageComponent, document.getElementById('root'));

