import React from 'react'
import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const AppNavBar = () => {
	return(

		<nav className="navbar navbar-expand-md navbar-light" style={{backgroundImage: "linear-gradient(to right, #585656, #343131)", borderBottom: '2px solid #0eff7d'}}>
		  	<a className="navbar-brand" href="/" style={{color: "white"}}><strong><span style={{color: "#0eff7d"}}>J</span>Preyes.com</strong></a>
		  	<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    	<span className="navbar-toggler-icon"></span>
		  	</button>
		  
		  	<div className="collapse navbar-collapse" id="navbarNav">
		    	<ul className="navbar-nav mx-auto">

			      	<li className="nav-item mx-4">
			        	<a className="nav-link" href="/" id={styles.mynavlinks}>Home</a>
			      	</li>
			      	<li className="nav-item mx-4">
			        	<a className="nav-link" href="/projects" id={styles.mynavlinks}>Projects</a>
			      	</li>
			      	<li className="nav-item mx-4">
			        	<a className="nav-link" href="/contact" id={styles.mynavlinks}>Contact Me</a>
			      	</li>
	
		    	</ul>
		  	</div>
		</nav>

	)
}

export default AppNavBar