import React from 'react'
import ReactDOM from 'react-dom'
import AppNavBar from './AppNavBar'
import styles from './mystyle.module.css'

const HomePage = () => {
	return(
		<React.Fragment>
		<AppNavBar/>

		<div className="container-fluid">
			
			<div className="row" id={styles.landing}>
				<div className="col-md-5 offset-md-7">
					<h2 className={styles.greeting}>Hi I'm <span style={{color: "#0eff7d", fontWeight: "bolder"}}>Pat</span>!</h2>
					<hr style={{backgroundColor: "#fff", width: '50%', margin: 'auto', fontWeight: 'bolder'}}/>
					<p className={styles.landingquote}>"getting a quality website is not an expenses but rather an investment"</p>
					<p className="text-center"><small style={{color: 'white', fontSize: '1rem'}}>- Dr. Christopher Dayagdag</small></p>

					<p className="text-center">
						<a href="/projects" className="btn btn-lg btn-primary mx-1" id={styles.landingprojectbutton}>My Projects</a>
						<a href="/contact" className="btn btn-lg btn-primary mx-1" id={styles.landingcontactbutton}>Contact Me</a>
					</p>
				</div>
			</div>

		</div>




		<div className="container-fluid">
			<div className="row" id={styles.homeMe} data-aos="fade-right">
				<div className="col-md-5 offset-md-1">
					<h2 className={styles.homeMename}>Jan <span style={{color: "#0eff7d", fontWeight: "bolder"}}>Pat</span>rick Reyes</h2>
					<hr style={{backgroundColor: "#fff", width: '90%', margin: 'auto', fontWeight: 'bolder', marginBottom: '5%'}}/>
					<p className={styles.profile}>Hello! I'm Jan Patrick Reyes! And I'm a Web Designer and Developer.</p>
					<p className={styles.profile}>I studied in the Polytechnic University of the Philippines and I took a course of Bachelor of Science in Information Technology.</p>
					<p className={styles.profile}>I also went to Zuitt Coding Bootcamp to study Full-Stack Web Development for 3 months.</p>
					<p className={styles.profile}>I have a diverse set of skills regarding the Web Development languages such as: HTML, CSS, Bootstrap, JavaScript, ReactJS, all the way to PHP and more!</p>
				</div>
			</div>
		</div>




		<div className="container-fluid">
			<div className="row" id={styles.morehomeMe} data-aos="fade-left">
				<div className="col-md-4 offset-md-7">
					<h2 className={styles.homeMename}>Get to know me <span style={{color: "#0eff7d", fontWeight: "bolder"}}>More!</span></h2>
					<hr style={{backgroundColor: "#fff", width: '90%', margin: 'auto', fontWeight: 'bolder', marginBottom: '5%'}}/>
					<p className={styles.profile}>I really love sports especially basketball. Basketball is already in my blood and I can't live without it!</p>
					<p className={styles.profile}>Playing the guitar is one of my passion! The famous guitarist that I really look up to is Sungha Jung because I really like his playing style!</p>
					<p className={styles.profile}>My weakness? Dogs! I really love dogs especially the Corgis! Dogs are one of the source of my strength to stay awake while coding!</p>
				</div>
			</div>
		</div>




		<div className="container-fluid">
			<div className="row" id={styles.skills} data-aos="fade-right">
				<div className="col-md-8 mx-auto">
					<h2 className={styles.skillsheading}>What I use in my <span style={{color: "#0eff7d", fontWeight: "bolder"}}>projects</span>?</h2>
					<hr style={{borderTop: '2px solid black', width: '70%', margin: 'auto'}}/>
				</div>
			</div>
		</div>










		</React.Fragment>
	)
}
export default HomePage